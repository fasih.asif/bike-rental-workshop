package io.axoniq.demo.bikerental.rental.query;

import io.axoniq.demo.bikerental.coreapi.rental.BikeInUseEvent;
import io.axoniq.demo.bikerental.coreapi.rental.BikeRegisteredEvent;
import io.axoniq.demo.bikerental.coreapi.rental.BikeRequestedEvent;
import io.axoniq.demo.bikerental.coreapi.rental.BikeReturnedEvent;
import io.axoniq.demo.bikerental.coreapi.rental.BikeStatus;
import io.axoniq.demo.bikerental.coreapi.rental.RequestRejectedEvent;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.queryhandling.QueryHandler;
import org.springframework.stereotype.Component;

@Component
public class BikeStatusProjection {

    private final BikeStatusRepository bikeStatusRepository;

    public BikeStatusProjection(BikeStatusRepository bikeStatusRepository) {
        this.bikeStatusRepository = bikeStatusRepository;
    }

    @EventHandler
    public void on(BikeRegisteredEvent bikeRegisteredEvent) {
        var bikeStatus = new BikeStatus(bikeRegisteredEvent.bikeId(), bikeRegisteredEvent.bikeType(), bikeRegisteredEvent.location());
        bikeStatusRepository.save(bikeStatus);
    }

    @EventHandler
    public void on(BikeRequestedEvent bikeRequestedEvent) {
        bikeStatusRepository.findById(bikeRequestedEvent.bikeId())
                .map(bikeStatus -> {
                    bikeStatus.rentedBy(bikeRequestedEvent.renter());
                    return bikeStatus;
                });
    }

    @EventHandler
    public void on(BikeReturnedEvent bikeReturnedEvent) {
        bikeStatusRepository.findById(bikeReturnedEvent.bikeId())
                .map(bikeStatus -> {
                    bikeStatus.returnedAt(bikeReturnedEvent.location());
                    return bikeStatus;
                });
    }


    @EventHandler
    public void on(RequestRejectedEvent requestRejectedEvent) {
        bikeStatusRepository.findById(requestRejectedEvent.bikeId())
                .map(bikeStatus -> {
                    bikeStatus.returnedAt(bikeStatus.getLocation());
                    return bikeStatus;
                });
    }

    @EventHandler
    public void on(BikeInUseEvent bikeInUseEvent) {
        bikeStatusRepository.findById(bikeInUseEvent.bikeId())
                .map(bikeStatus -> {
                    bikeStatus.rentedBy(bikeInUseEvent.renter());
                    return bikeStatus;
                });
    }

    //Query Handlers

    @QueryHandler(queryName = "findAll")
    public Iterable<BikeStatus> findAll() {
        return bikeStatusRepository.findAll();
    }

    @QueryHandler(queryName = "findOne")
    public BikeStatus findOne(String bikeId) {
        return bikeStatusRepository.findById(bikeId).orElse(null);
    }
}
