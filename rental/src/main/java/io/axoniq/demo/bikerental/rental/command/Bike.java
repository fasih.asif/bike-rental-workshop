package io.axoniq.demo.bikerental.rental.command;

import io.axoniq.demo.bikerental.coreapi.rental.ApproveRequestCommand;
import io.axoniq.demo.bikerental.coreapi.rental.BikeInUseEvent;
import io.axoniq.demo.bikerental.coreapi.rental.BikeRegisteredEvent;
import io.axoniq.demo.bikerental.coreapi.rental.BikeRequestedEvent;
import io.axoniq.demo.bikerental.coreapi.rental.BikeReturnedEvent;
import io.axoniq.demo.bikerental.coreapi.rental.RegisterBikeCommand;
import io.axoniq.demo.bikerental.coreapi.rental.RejectRequestCommand;
import io.axoniq.demo.bikerental.coreapi.rental.RequestBikeCommand;
import io.axoniq.demo.bikerental.coreapi.rental.RequestRejectedEvent;
import io.axoniq.demo.bikerental.coreapi.rental.ReturnBikeCommand;
import lombok.NoArgsConstructor;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.spring.stereotype.Aggregate;

import java.util.UUID;

import static org.axonframework.modelling.command.AggregateLifecycle.apply;

@Aggregate
@NoArgsConstructor
public class Bike {

    @AggregateIdentifier
    String bikeId;

    boolean available;
    String renter;

    boolean confirmed;

    //Command Handlers
    @CommandHandler
    public Bike(RegisterBikeCommand registerBikeCommand) {
        apply(new BikeRegisteredEvent(registerBikeCommand.bikeId(), registerBikeCommand.bikeType(), registerBikeCommand.location()));
    }

    @CommandHandler
    public String handle(RequestBikeCommand requestBikeCommand) {
        if (!this.available) {
            throw new IllegalStateException("Bike is already reserved!");
        }

        String rentalReference = UUID.randomUUID().toString();

        apply(new BikeRequestedEvent(requestBikeCommand.bikeId(), requestBikeCommand.renter(), rentalReference));

        return rentalReference;
    }

    @CommandHandler
    public void handle(ReturnBikeCommand returnBikeCommand) {
        if (this.available) {
            throw new IllegalStateException("Bike was already returned");
        }

        apply(new BikeReturnedEvent(returnBikeCommand.bikeId(), returnBikeCommand.location()));
    }

    @CommandHandler
    public void handle(RejectRequestCommand rejectRequestCommand) {
        if (!(rejectRequestCommand.renter().equalsIgnoreCase(renter)) || confirmed) {
            return;
        }
        apply(new RequestRejectedEvent(rejectRequestCommand.bikeId()));
    }

    @CommandHandler
    public void handle(ApproveRequestCommand approveRequestCommand) {
        if (!(approveRequestCommand.renter().equalsIgnoreCase(renter)) || confirmed) {
            return;
        }

        apply(new BikeInUseEvent(approveRequestCommand.bikeId(), approveRequestCommand.renter()));
    }

    //Event Sourcing Handlers

    @EventSourcingHandler
    public void processBikeRegisteredEvents(BikeRegisteredEvent bikeRegisteredEvent) {
        this.bikeId = bikeRegisteredEvent.bikeId();
        this.available = true;
    }

    @EventSourcingHandler
    public void processBikeRequestedEvents(BikeRequestedEvent bikeRequestedEvent) {
        this.renter = bikeRequestedEvent.renter();
        this.confirmed = false;
        this.available = false;
    }

    @EventSourcingHandler
    public void processBikeReturnedEvents(BikeReturnedEvent bikeReturnedEvent) {
        this.renter = null;
        this.confirmed = false;
        this.available = true;
    }

    @EventSourcingHandler
    public void processRequestRejectedEvents(RequestRejectedEvent requestRejectedEvent) {
        this.confirmed = false;
        this.renter = null;
        this.available = true;
    }

    @EventSourcingHandler
    public void processBikeInUseEvents(BikeInUseEvent bikeInUseEvent) {
        this.available = false;
        this.confirmed = true;
    }
}