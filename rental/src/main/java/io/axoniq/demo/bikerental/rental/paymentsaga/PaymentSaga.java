package io.axoniq.demo.bikerental.rental.paymentsaga;

import io.axoniq.demo.bikerental.coreapi.payment.PaymentConfirmedEvent;
import io.axoniq.demo.bikerental.coreapi.payment.PreparePaymentCommand;
import io.axoniq.demo.bikerental.coreapi.rental.ApproveRequestCommand;
import io.axoniq.demo.bikerental.coreapi.rental.BikeRequestedEvent;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.modelling.saga.EndSaga;
import org.axonframework.modelling.saga.SagaEventHandler;
import org.axonframework.modelling.saga.SagaLifecycle;
import org.axonframework.modelling.saga.StartSaga;
import org.axonframework.spring.stereotype.Saga;
import org.springframework.beans.factory.annotation.Autowired;

@Saga
public class PaymentSaga {

    @Autowired
    private transient CommandGateway commandGateway;

    String bikeId;

    String renter;

    @StartSaga
    @SagaEventHandler(associationProperty = "bikeId")
    public void on(BikeRequestedEvent bikeRequestedEvent) {
        this.bikeId = bikeRequestedEvent.bikeId();
        this.renter = bikeRequestedEvent.renter();

        String paymentId = bikeRequestedEvent.rentalReference();

        SagaLifecycle.associateWith("paymentId", paymentId);

        //Send a Prepare Payment Command via the Command Gateway
        commandGateway.send(new PreparePaymentCommand(100, paymentId));
    }

    @EndSaga
    @SagaEventHandler(associationProperty = "paymentId")
    public void on(PaymentConfirmedEvent paymentConfirmedEvent) {
        //The payment is successful, so we approve the bike request!!!
        commandGateway.send(new ApproveRequestCommand(bikeId, renter));
    }
}